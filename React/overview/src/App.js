import React, {useState} from "react";
import "./App.scss";

function App() {
    const [age, setAge] = useState(21);
    const ageUpHandler = () => {
        setAge(age + 1);
    }
    const ageDownHandler = () => {
      setAge(age - 1);
    };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Use State Hook</h1>
        <h2>Age: {age}</h2>
        <button onClick={ageUpHandler}>Age up</button>
        <button onClick={ageDownHandler}>Age down</button>
      </header>
    </div>
  );
}

export default App;