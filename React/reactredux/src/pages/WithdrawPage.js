import React from "react";
import { useSelector, useDispatch } from "react-redux";

const WithdrawPage = () => {
  const balance = useSelector(state => state.balanceReducer.balance);
  const loan = useSelector(state => state.loanReducer.loan);
  const dispatch = useDispatch();
  const withdrawClickHandle = e => {
    // if (balance - 10 < 0) {
    //   alert("You don't have enough balance to withdraw");
    //   return;
    // }
    dispatch({ type: "WITHDRAW", payload: 10 });
  };

  return (
    <div>
      <h1> balance: {balance} </h1>
      <button onClick={withdrawClickHandle}>Withdraw</button>
      <h1>{loan ? "Loan Applied" : "No Loan"}</h1>
    </div>
  );
};

export default WithdrawPage;
