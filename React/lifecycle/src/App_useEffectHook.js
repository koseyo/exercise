import React, {useEffect, useState} from 'react';
import './App.css';

let born = false;

function App() {
  const [growth, setGrowth] = useState(0);
  const [nirvana, setNirvana] = useState(false);

  useEffect(() => {
    if(born) {
      document.title = "Nirvana attained"
    }
  }, [nirvana]);

  useEffect(() => {
    console.log("I am born");
  }, []);

  useEffect(() => {
    if(born) {
      console.log("make mistake and learn");
    } else {
      born = true;
    }

    if(growth > 70) {
      setNirvana(true);
    }
  }, [growth]);

  const growHandle = () => {
    setGrowth(growth + 10);
  }

  return (
    <div className="App">
      <header className="App-header">
        <h2>Use Effect</h2>
        <h3>growth:{growth}</h3>
        <button onClick={growHandle}>learn and grow</button>
      </header>
    </div>
  );
}

export default App;
