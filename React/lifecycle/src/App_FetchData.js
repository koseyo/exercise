import React, { useEffect, useState } from "react";
import "./App.css";

const initProfile = {
  id: null,
  publicRepos: null
};

function App() {
  const [profile, setProfile] = useState(initProfile);

  const getProfile = async () => {
    const response = await fetch(`https://api.github.com/users/syoonk`);
    const json = await response.json();

    setProfile({
      id: json.id,
      publicRepos: json.public_repos
    });
  }

  useEffect(() => {
    getProfile();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h2>Fetch Data</h2>
        <h3>{`id: ${profile.id}, Public Repos: ${profile.publicRepos}`}</h3>
      </header>
    </div>
  );
}

export default App;
