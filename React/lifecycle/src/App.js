// Showing Error is not working

import React, { useState } from "react";
import "./App.css";
import useCustomFetch from "./hooks/useCustomFetch";

function App() {
  const [url, setUrl] = useState(null);
  const [data, loading, error] = useCustomFetch(url);

  const getPublicRepos = e => {
    if (e.key === "Enter") {
      setUrl("https://api.github.com/users/" + e.target.value);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <h2>
          GitID : <input onKeyPress={getPublicRepos}></input>
          {loading && url && <div>loading....</div>}
          {!loading && data && data.public_repos && (
            <div>public_repos: {data.public_repos}</div>
          )}
          {error && <div>Error: {error}</div>}
        </h2>
      </header>
    </div>
  );
}

export default App;
