/*
    1. Show the x,y location within every mouse move.
    2. Referesh the Date every second.
*/

import React, { useEffect, useState } from "react";
import "./App.css";

const initXY = {
  x: null,
  y: null
};

function App() {
  const [time, setTime] = useState(Date);
  const [xy, setXY] = useState(initXY);

  const mousemoveHandle = e => {
    setXY({
      x: e.clientX,
      y: e.clientY
    });
  };

  // This effect should be called only once.
  useEffect(() => {
    window.addEventListener("mousemove", mousemoveHandle);

    // It is another way via cleaning up instead of giving empty array.
    // return () => {
    //   window.removeEventListener("mousemove", mousemoveHandle);
    // };
  }, []);

  useEffect(() => {
    const timeHandle = setInterval(() => {
      setTime(Date);
    }, 1000);

    return () => {
      clearInterval(timeHandle);
    };
  });

  return (
    <div className="App">
      <header className="App-header">
        <h2>Use Effect Examples</h2>
        <h3>Date & Time : {time}</h3>
        <h3>{`x:${xy.x}, y:${xy.y}`}</h3>
      </header>
    </div>
  );
}

export default App;
