import React, { useState, useEffect } from "react";
import "./App.css";

const initialMatrix = [];

function App() {
  const [matrix, setMatrix] = useState(initialMatrix);
  const [matrixSize, setMatrixSize] = useState(3);
  const [currentPlayer, setCurrentPlayer] = useState("o");
  const [selRow, setSelRow] = useState(null);
  const [selCol, setSelCol] = useState(null);
  const [winner, setWinner] = useState(false);
  const [reset, setReset] = useState(false);

  useEffect(() => {
    
    setWinner(false);
    setSelCol(null);
    setSelRow(null);

    const row = new Array(matrixSize).fill(null);

    const tempMatrix = new Array();

    for (let i = 0; i < matrixSize; i++) {
      tempMatrix.push([...row]);
    }

    setMatrix(tempMatrix);
  }, [reset]);

  const squareClickHandle = (rw, cl) => {
    if (!matrix[rw][cl] && !winner) {
      // it allows to use information from all functon of here.
      setSelCol(cl);
      setSelRow(rw);
      let nextPlayer = currentPlayer === "x" ? "o" : "x";
      setCurrentPlayer(nextPlayer);
      const matrixCopy = matrix;
      matrixCopy[rw][cl] = nextPlayer;
      setMatrix(matrixCopy);
    }
  };

  const isWinner = () => {
    let vertical = true;
    let horizontal = true;
    let diagonal1 = true;
    let diagonal2 = true;

    if(selCol === null || selRow === null)  return;

    for(let i = 0; i < matrixSize; i++) {
      if(matrix[i][selCol] !== currentPlayer) {
        vertical = false;
      }
      if(matrix[selRow][i] !== currentPlayer) {
        horizontal = false;
      }
      if(matrix[i][i] !== currentPlayer) {
        diagonal1 = false;
      }
      if(matrix[i][matrixSize -1 -i] !== currentPlayer) {
        diagonal2 = false;
      }
    }
    
    if (vertical || horizontal || diagonal1 || diagonal2) {
      setWinner(true);
    }
  };

  useEffect(() => {
    if(!winner) {
      isWinner();
    }
  }); 

  const resetGameHandle = () => {
    setReset(!reset);
  } 

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={resetGameHandle}>Reset Game</button>
        <div>
          {matrix.map((el, cl) => (
            <div className="col">
              {el.map((el2, rw) => (
                <div onClick={() => squareClickHandle(rw, cl)} className="row">
                  {matrix[rw][cl]}
                </div>
              ))}
            </div>
          ))}
        </div>
        <h2>{winner ? `Player ${currentPlayer} is winner` : ""}</h2>
      </header>
    </div>
  );
}

export default App;
