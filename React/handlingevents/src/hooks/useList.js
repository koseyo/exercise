import { useState } from "react";

function useList(_init) {
  const [list, setList] = useState(_init);

  return {
      list, 
    removeItem(_item) {
      const filteredList = list.filter(el => el.name !== _item);
      setList(filteredList);
    },
    saveItem(_index, _newValue) {
      const copyList = [...list];
      copyList[_index].name = _newValue;
    }
  };
}

export default useList;
