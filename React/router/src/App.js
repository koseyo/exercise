import React, { useState } from "react";
import {
  BrowserRouter,
  Route,
  Link,
  NavLink,
  Redirect,
  Prompt
} from "react-router-dom";
import "./App.css";
import HomePage from "./pages/HomePage.js";
import AboutPage from "./pages/AboutPage.js";
import messageContext from "./contexts/messageContext";

function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [age, setAge] = useState(null);
  const [message, setMessage] = useState("I am being shared");

  const loginClickHandle = () => {
    setLoggedIn(!loggedIn);
  };

  const onChangeHandle = e => {
    setAge(e.target.value);
  };

  return (
    <BrowserRouter>
      <messageContext.Provider value={[message, setMessage]}>
        <div className="App">
          <header className="App-header">
            <ul className="ul-style">
              <li className="li-style">
                <NavLink
                  className="App-link"
                  to="/"
                  exact
                  activeClassName="Link-active-style"
                >
                  Home
                </NavLink>
              </li>
              <li className="li-style">
                <NavLink
                  className="App-link"
                  to="/about"
                  exact
                  activeClassName="Link-active-style"
                >
                  About
                </NavLink>
              </li>
              <li className="li-style">
                <NavLink
                  className="App-link"
                  to="/user/Sungyoon/Kong"
                  exact
                  activeClassName="Link-active-style"
                >
                  Sungyoon Kong
                </NavLink>
              </li>
            </ul>

            <Prompt
              when={loggedIn && !age}
              message={location => {
                return location.pathname.startsWith("/user")
                  ? true
                  : "Are you sure?";
              }}
            ></Prompt>

            <button onClick={loginClickHandle} className="button">
              {loggedIn ? "logout" : "login"}
            </button>

            <Route
              path="/"
              exact
              component={HomePage}
            ></Route>
            <Route path="/about" exact component={AboutPage}></Route>
            <Route
              path="/user/:firstname/:lastname"
              exact
              render={({ match }) => {
                return loggedIn ? (
                  <h1>
                    <h2>age: {age}</h2>
                    <input
                      type="number"
                      value={age}
                      onChange={onChangeHandle}
                    ></input>
                    Welcome {match.params.firstname} {match.params.lastname}
                  </h1>
                ) : (
                  <Redirect to="/"></Redirect>
                );
              }}
            ></Route>
          </header>
        </div>
      </messageContext.Provider>
    </BrowserRouter>
  );
}

export default App;
