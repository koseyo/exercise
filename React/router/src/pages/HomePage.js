import React, { useContext } from "react";
import messageContext from "./../contexts/messageContext.js";

const HomePage = () => {
  return (
    <>
      <h1>Welcome Home</h1>
      <h2>Message: {useContext(messageContext)}</h2>
    </>
  );
};

export default HomePage;
