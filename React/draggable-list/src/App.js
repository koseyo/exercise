import React, { useState } from "react";
import "./App.css";

const initList = [1, 2, 3, 4, 5, 6];

function App() {
  const [list, setList] = useState(initList);
  const [draggedItem, setDraggedItem] = useState(null);

  const dragStartHandle = (e, idx) => {
    setDraggedItem(list[idx]);
    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData('text.html', e.target.parentNode);
    e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
  };

  const dragOverHandle = idx => {
    const draggedOverItem = list[idx];

    if(draggedOverItem === draggedItem) {
      return;
    }

    const filteredList = list.filter( el => el !== draggedItem);

    filteredList.splice(idx, 0, draggedItem);
    
    setList(filteredList);
  }

  return (
    <div className="App">
      <header className="App-header">
        <h3>Drag and Drop Project</h3>
        <ul>
          {list.map((el, idx) => {
            return (
              <li key={idx} className="item-style">
                <div draggable onDragOver={e => dragOverHandle(idx)} onDragStart={e => dragStartHandle(e, idx)}>
                  {el}
                </div>
              </li>
            );
          })}
        </ul>
      </header>
    </div>
  );
}

export default App;
