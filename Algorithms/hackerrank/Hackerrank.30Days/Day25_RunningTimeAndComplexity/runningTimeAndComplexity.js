function processData(input) {
  const arr = input.split("\n").map(el => Number(el));

  for (let i = 1; i <= arr[0]; i++) {
    let prime = true;

    if (arr[i] === 1) {
      console.log(`Not prime`);
      continue;
    }

    let div = 2;
    while (div <= Math.floor(arr[i] / 2)) {
      if (arr[i] % div === 0) {
        prime = false;
        break;
      }

      div++;
    }

    prime ? console.log(`Prime`) : console.log('Not prime');
  }
} 
