function processData(input) {
  /*
    dates[1]: Current Date,
    dates[2]: Expected Date
  */
  let dates = [];
  (input.split("\n")).map(el => {
    dates.push(el.split(" ").map(toN => Number(toN)));
  });

  if (dates[0][2] > dates[1][2]) {
    console.log(10000);
    return;
  } else if (dates[0][2] < dates[1][2]) {
    console.log(0);
    return;
  }

  let fine = 0;
  const fines = [15, 500];  // fine factors [day, month]
  for (let i = 1; i >= 0; i--) {
    if (dates[0][i] <= dates[1][i]) {
      continue;
    } else {
      fine = fines[i] * (dates[0][i] - dates[1][i]);
      break;
    }
  }

  console.log(fine);
} 
