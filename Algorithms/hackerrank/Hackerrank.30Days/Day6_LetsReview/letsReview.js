/*
    Given a string, S , of length N that is indexed from 0 to N-1, 
    print its even-indexed and odd-indexed characters as 2 space-separated strings on a single line.
 */

function processData(input) {
    var newString = [];
    newString[0] = '';

    var evenS = '';
    var oddS = '';
    var N;
    var sN = 0;

    // Get lines for ignoring \n
    for (let i = 0; i < input.length; i++) {
        if (input[i] == '\n') {
            sN++;
            newString[sN] = '';
            continue;
        }
        newString[sN] += input[i];
    }

    N = parseInt(newString[0]); // N is number of lines given in first line of input.

    for (let sN = 1; sN <= N; sN++) {
        for (let i = 0; i < newString[sN].length; i++) {
            (i % 2) ? (oddS += newString[sN][i]) : (evenS += newString[sN][i]);
        }

        console.log(evenS + ' ' + oddS);

        oddS = '';
        evenS = '';
    }
}