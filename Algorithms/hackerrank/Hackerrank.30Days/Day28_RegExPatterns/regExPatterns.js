'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

function checkAddress(_firstName, _email) {
  let isMatch = false;
  
  if(_email.search(/@gmail.com$/) > 0) {
    isMatch = true;
  }

  return isMatch;
}

function main() {
    const N = parseInt(readLine(), 10);
    let matchList = [];

    for (let NItr = 0; NItr < N; NItr++) {
        const firstNameEmailID = readLine().split(' ');

        const firstName = firstNameEmailID[0];

        const emailID = firstNameEmailID[1];

        if(checkAddress(firstName, emailID)) {
          matchList.push(firstName);
        }
    }
    matchList.sort();

    for(let i = 0; i < matchList.length; i++) {
      console.log(matchList[i]);
    }
}
