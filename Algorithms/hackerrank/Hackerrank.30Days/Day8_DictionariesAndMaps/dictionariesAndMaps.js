/*
    Given n names and phone numbers, assemble a phone book that maps friends' 
    names to their respective phone numbers. You will then be given an unknown number 
    of names to query your phone book for. For each name queried, print 
    the associated entry from your phone book on a new line in the form name=phoneNumber; 
    if an entry for name is not found, print Not found instead.

    example input :
    3
    sam 99912222
    tom 11122222
    harry 12299933
    sam
    edward
    harry

    from second to fourth line are phone book, compare it with below name lists.
    if there is no match it would be 'Not found'.

*/

function processData(input) {
    var phoneBook = [];
    var persons = [];
    input = input.split('\n');
    const N = parseInt(input[0]);

    for (let i = 1; i <= N; i++) {
        let dummy = input[i].split(' ');
        phoneBook[dummy[0]] = dummy[1];
    }

    for (let i = N + 1; i < input.length; i++) {
        if (phoneBook[input[i]] != undefined) {
            console.log(input[i] + '=' + phoneBook[input[i]]);
        } else {
            console.log('Not found');
        }
    }
} 