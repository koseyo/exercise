function freqQuery(queries) {
  let result = new Array(0);
  let freq = {};
  let data = {};

  for(let query of queries) {
    const [factor, value] = query;
    !data[value] && (data[value] = 0);
    (!freq[data[value]] || freq[data[value]] < 0)
      && (freq[data[value]] = 0);

    if(factor === 1) {
      freq[data[value]]--;
      data[value]++;
      !freq[data[value]] ?
        freq[data[value]] = 1 : freq[data[value]]++;
    }
    if(factor === 2 && data[value] > 0) {
      freq[data[value]]--;
      data[value]--;
      freq[data[value]]++;
    }
    if(factor === 3) {
      freq[value] > 0 ? result.push(1) : result.push(0);
    }
  }
}