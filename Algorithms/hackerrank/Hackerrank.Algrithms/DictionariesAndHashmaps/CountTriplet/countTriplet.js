function countTriplets(arr, r) {
  let counter = {};
  let count = 0;

  for(let el of arr) {
    let elR = el * r;
    !counter[el] && (counter[el] = { id2: 0, id3: 0 });
    !counter[elR] && (counter[elR] = { id2: 0, id3: 0 });

    count += counter[el].id3;
    counter[elR].id3 += counter[el].id2;
    counter[elR].id2++;
  }

  return count;
}