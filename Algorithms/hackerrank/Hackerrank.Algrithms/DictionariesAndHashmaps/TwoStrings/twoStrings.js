function twoStrings(s1, s2) {
    for (let ch of s1) {
        if (s2.includes(ch)) {
            return 'YES';
        }
    }

    return 'NO';
}