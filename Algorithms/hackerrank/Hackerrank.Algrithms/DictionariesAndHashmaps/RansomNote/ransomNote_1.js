/*
    Harold is a kidnapper who wrote a ransom note, but now he is worried it will be traced 
    back to him through his handwriting. He found a magazine and wants to know if he can 
    cut out whole words from it and use them to create an untraceable replica of his ransom 
    note. The words in his note are case-sensitive and he must use only whole words available 
    in the magazine. He cannot use substrings or concatenation to create the words he needs.

    Given the words in the magazine and the words in the ransom note, print Yes if he can 
    replicate his ransom note exactly using whole words from the magazine; otherwise, print No.

    For example, the note is "Attack at dawn". The magazine contains only "attack at dawn". 
    The magazine has all the right words, but there's a case mismatch. The answer is Yes.

    Input : 
    6 5
    two times three is not four
    two times two is four

    Output :
    No

*/

// It takes longer
function checkMagazine(magazine, note) {
    var ransom = note.reduce((r, no) => {
        var dummy = magazine.indexOf(no);

        if (dummy >= 0) {
            r = r + 1;
            magazine[dummy] = 0;

            return r;
        }
    }, 0);

    (ransom == note.length) ? console.log('Yes') : console.log('No');
}