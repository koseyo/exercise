function maxSubsetSum(arr) {
  const length = arr.length;
  let maxSum = new Array(length).fill(0);

  maxSum[0] = maxSum[0] < arr[0] ? arr[0] : maxSum[0];
  maxSum[1] = Math.max(arr[1], maxSum[0]);
  for(let i = 2; i < length; i++) {
    if(arr[i] <= 0) maxSum[i] = maxSum[i - 1];
    maxSum[i] = Math.max(arr[i], maxSum[i - 1], arr[i] + maxSum[i - 2]);
  }
  
  return Math.max(maxSum[length - 1]);
}