function maxMin(k, arr) {
  arr.sort((x, y) => x - y);

  let min = null;
  for(let i = 0; i < arr.length - (k - 1); i++) {
    let newValue = arr[i + k - 1] - arr[i];
    !min && (min = newValue);
    newValue < min && (min = newValue);
  }
  return min;
}