/*

    You are given an unordered array consisting of consecutive integers [1, 2, 3, ..., n] without any duplicates.
    You are allowed to swap any two elements. You need to find the minimum number of swaps required to
    sort the array in ascending order.

    For example, given the array arr = [7, 1, 3, 2, 4, 5, 6] we perform the following steps:

    i   arr                         swap (indices)
    0   [7, 1, 3, 2, 4, 5, 6]   swap (0,3)
    1   [2, 1, 3, 7, 4, 5, 6]   swap (0,1)
    2   [1, 2, 3, 7, 4, 5, 6]   swap (3,4)
    3   [1, 2, 3, 4, 7, 5, 6]   swap (4,5)
    4   [1, 2, 3, 4, 5, 7, 6]   swap (5,6)
    5   [1, 2, 3, 4, 5, 6, 7]

    It tooks 5 steps in minimum

*/

//This code takes time longer..
function minimumSwaps(arr) {
    var dummy;
    var dummyi;
    var nSwap = 0;

    for(let i=0; i<arr.length; i++) {
        if(arr[i] != i++) {
            dummy = arr[i];
            dummyi = arr.indexOf(i+1);

            arr[i] = i+1;
            arr[dummyi] = dummy;

            nSwap++;
        }
    }

    return nSwap;
}