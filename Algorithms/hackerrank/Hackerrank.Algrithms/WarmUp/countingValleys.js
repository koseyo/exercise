/*
    U is step for Up, D is step for Down. 
    Strat from sea level, if he goes under sea level it counts valley.
    If he goes up that is mountain, and valley will be count again if he goes under the sea level again.
    s is string such a s = {U, D, D, D, U ...}, n is number of steps.
*/

function countingValleys(n, s) {
    var valley = 0;
    var mountain = 0;
    var altitude = 0;

    for (let i = 0; i < n; i++) {
        if (altitude == 0) {
            (s[i] == 'U') ? mountain++ : valley++;
        }
    }

    return valley;
}