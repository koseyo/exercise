/*
    Emma is playing a new mobile game that starts with consecutively numbered clouds. Some of the clouds
    are thunderheads and others are cumulus. She can jump on any cumulus cloud having a number that is 
    equal to the number of the current cloud plus 1 or 2. She must avoid the thunderheads. 
    Determine the minimum number of jumps it will take Emma to jump from her starting postion 
    to the last cloud. It is always possible to win the game.

    For each game, Emma will get an array of clouds numbered 0 if they are safe or 1 if they must be avoided.

    if C = [0, 1, 0, 0, 0, 1, 0]
    avoid index 1 and 5, there are two path, 0 2 4 6 or 0 2 3 4 6.
    former one is the shortest path with 3 steps. 
*/

function checker(a, b) {
    if (b == 0) { return 2; }
    else { return 1; }
}

function jumpingOnClouds (c) {
    const N = c.length;
    var jumps = 0;
    var site = 0;

    while (true) {
        if (site == N - 3 || site == N - 2) {
            jumps++;
            return jumps;
        }

        site += checker (c[site + 1], c[site + 2]);
        jumps++;
    }
}