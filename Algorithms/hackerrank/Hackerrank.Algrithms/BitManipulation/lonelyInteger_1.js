/*

    Every integer n in Array a has a pair except one. Find out one has no pair.

    For example, s = [1, 1, 2, 2, 3, 4, 4, 5, 5]
    It is 3.

    Constraints::
    1<=n<100
    It is guaranteed n is odd number
    0<=a[i]<=100

*/


function lonelyinteger(a) {
    var pairOf = [];
    var lonely;

    for(let i in a) {
        if(pairOf[a[i]] == 0 || pairOf[a[i]] == undefined) {
            pairOf[a[i]] = 1;
        } else {
            pairOf[a[i]]++;
        }
    }

    for(let i in pairOf) {
        if(pairOf[i] == 1)  {
            return i;
        }
    }
}