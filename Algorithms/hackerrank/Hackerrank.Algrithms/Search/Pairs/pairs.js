function pairs(k, arr) {
  arr.sort((x, y) => x - y);

  let count = 0;
  for(let i = 0; i < arr.length - 1; i++) {
    for(let j = i + 1; j < arr.length; j++) {
      const diff = arr[j] - arr[i];
      if(diff > k) break;
      if(diff === k) {
        count++;
        break;
      }
    }
  }
  return count;
}