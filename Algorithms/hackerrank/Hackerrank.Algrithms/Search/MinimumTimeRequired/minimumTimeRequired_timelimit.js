function minTime(machines, goal) {
  let product = 0;
  let days = 1;
  while(product < goal) {
    for(let machine of machines) {
      if(days % machine === 0)  product++;
    }
    days++;
  }
  return days - 1;
}