function minTime(machines, goal) {
  machines.sort((x, y) => x - y);
  const numOfMachines = machines.length;
  let lowerBound = Math.ceil(goal / (numOfMachines / machines[0]));
  let upperBound = Math.ceil(goal / (numOfMachines / machines[numOfMachines - 1]));

  while(lowerBound < upperBound) {
    const assumeDay = parseInt((lowerBound + upperBound) / 2);
    
    const products = machines.reduce((acc, machine) => {
      return acc += Math.floor(assumeDay / machine);
    }, 0);

    products >= goal ? upperBound = assumeDay : lowerBound = assumeDay + 1;
  }

  return lowerBound;
}