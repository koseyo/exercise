function countSwaps(a) {
  let isSorted = false;
  let totalSwaps = 0;

  while(!isSorted) {  
    let swaps = 0;

    for(let i = 0; i < a.length - 1; i++) {
      if(a[i] > a[i + 1]) {
        let dummy = a[i];
        a[i] = a[i + 1];
        a[i + 1] = dummy;
        swaps++;
      }
    }
    swaps === 0 ? isSorted = true : totalSwaps += swaps;
  }
  console.log(`Array is sorted in ${totalSwaps} swaps.`);
  console.log(`First Element: ${a[0]}`);
  console.log(`Last Element: ${a[a.length - 1]}`);
}