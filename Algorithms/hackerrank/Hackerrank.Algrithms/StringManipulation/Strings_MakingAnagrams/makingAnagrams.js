function makeAnagram(a, b) {
  // a: 0, b: 1
  let counting = Array.from(Array(26), () => Array(2).fill(0));
  const aArr = Array.from(a);
  const bArr = Array.from(b);

  for(let aEl of aArr) {
    counting[aEl.charCodeAt(0) - 97][0]++;
  }
  for(let bEl of bArr) {
    counting[bEl.charCodeAt(0) - 97][1]++;
  }

  let deletion = 0;
  for(let el of counting) {
    deletion += Math.abs(el[0] - el[1]);
  }

  return deletion;
}