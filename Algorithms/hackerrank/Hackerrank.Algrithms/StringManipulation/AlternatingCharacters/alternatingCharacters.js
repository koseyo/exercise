function alternatingCharacters(s) {
  const arr = Array.from(s);

  let count = 0;
  for(let i = 1; i < arr.length; i++) {
    if(arr[i - 1] === arr[i]) {
      count++;
    }
  }

  return count;
}