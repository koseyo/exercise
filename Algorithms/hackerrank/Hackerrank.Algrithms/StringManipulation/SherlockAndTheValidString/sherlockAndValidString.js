function isValid(s) {
  const strings = Array.from(s);
  let count = {};
  let removerUsed = false;

  for(let str of strings) {
    !count[str] ? 
      count[str] = 1 : count[str]++;
  }

  let previousValue = 0;
  for(let str in count) {
    const strs = count[str];
    previousValue === 0 && (previousValue = strs);
    if(previousValue != strs) {
      if((Math.abs(previousValue - strs) === 1 || strs === 1) 
      && !removerUsed) {
        removerUsed = true;
      } else {
        return 'NO';
      }
    }
  }
  return 'YES';
}