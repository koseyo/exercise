function substrCount(n, s) {
  let count = n;

  for(let i = 0; i < n; i++) {
    let matchedIndex = 1;
    while(s[i] === s[i + matchedIndex]) matchedIndex++;

    if(matchedIndex > 1) {
      // palindrome number of same alphabet iterated, 
      // N(N+1)/2, 1+2+3+4+ ... + N = N(N+1)/2
      // count already n and single letter will not be counted.
      count += matchedIndex * (matchedIndex - 1) / 2;
      i = i + matchedIndex - 1;
    }
    if(matchedIndex === 1) {
      let checkingStep = 1;
      // Considering special palindrome. 
      // Here the case is abcba is not the palindrome. 
      // aacaa form only allowed for palindrome.
      while(s[i + checkingStep] === s[i - checkingStep]
      && s[i + checkingStep] === s[i + 1]
      && s[i + checkingStep] === s[i - 1]) {
        count++;
        checkingStep++;
      }
    }
  }

  return count;
}