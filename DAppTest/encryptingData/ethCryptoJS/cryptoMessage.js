const EthCrypto = require('eth-crypto');

// I gave them keys myself so it doesn't need...
const alice = EthCrypto.createIdentity();
const bob = EthCrypto.createIdentity();

const alicePrivateKey = '0x908e9f8fa5272efe0bbddf23e451e2d24d86a1433c0c73d12d518668cafcf148';
const bobPrivateKey = '0xab9876e391ce92c23581d1f368b88ef983b4f177f97bf1f044b42c7c51995f3b';
const alicePublicKey = EthCrypto.publicKeyByPrivateKey(alicePrivateKey);
const bobPublicKey = EthCrypto.publicKeyByPrivateKey(bobPrivateKey);
const aliceAddress = EthCrypto.publicKey.toAddress(alicePublicKey);
const bobAddress = EthCrypto.publicKey.toAddress(bobPublicKey);


async function ddong() {
  const messageGenerator = (_from, _to, _content) => {
    return {
      from: _from,
      to: _to,
      content: _content
    };
  }

  console.log(alicePublicKey);
  console.log(bobPublicKey);
  console.log(aliceAddress);
  console.log(bobAddress);


  const message = messageGenerator(aliceAddress, bobAddress, "What are you");
  console.log(message);
  const aliceSignature = await EthCrypto.sign(alicePrivateKey, EthCrypto.hash.keccak256(message));
  const messagePayload = {
    message: message,
    aliceSignature
  };
  const messageEncrypted = await EthCrypto.encryptWithPublicKey(bobPublicKey, JSON.stringify(messagePayload));
  console.log("encryptedMessage: ", messageEncrypted);
  const messageEncryptedString = await EthCrypto.cipher.stringify(messageEncrypted);
  console.log(messageEncryptedString);

  return messageEncryptedString;
}

async function ddongReceiver(_content) {
  const messageEncryptedObject = await EthCrypto.cipher.parse(_content);
  console.log(messageEncryptedObject);
  const messageDecrypted = await EthCrypto.decryptWithPrivateKey(bobPrivateKey, messageEncryptedObject);
  const payloadUnstringify = JSON.parse(messageDecrypted);
  console.log(payloadUnstringify);
  
  // Check signature
  const senderAddress = EthCrypto.recover(
    payloadUnstringify.aliceSignature,
    EthCrypto.hash.keccak256(payloadUnstringify.message)
  );
  
  console.log(senderAddress, ":: ", payloadUnstringify.message);
}

const content = ddong();
ddongReceiver(content);