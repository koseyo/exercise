const Web3 = require('web3');
const HandlingEvent = require('./build/contracts/HandlingEvent.json');

const init = async () => {
  const web3 = new Web3("http://localhost:8545");
  const networkId = await web3.eth.net.getId();
  const deployedNetwork = HandlingEvent.networks[networkId];
  const contract = new web3.eth.Contract(
    HandlingEvent.abi,
    deployedNetwork.address);
  const accounts = await web3.eth.getAccounts();

  const receipt = await contract.methods.sendMessage(accounts[1], "account[0] send1").send({from: accounts[0]});
  await contract.methods.sendMessage(accounts[2], "accounts[0] send1").send({from: accounts[0]});
  console.log(receipt.events);

  const allEvents = await contract.getPastEvents('NewMessage', {fromBlock: 0});
  console.log("Events000000");
  console.log(allEvents[0]);
  console.log("Events111111");
  console.log(allEvents[1]);
  console.log("====================");
  console.log(allEvents[0].returnValues.from);
}

init();